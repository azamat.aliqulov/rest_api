import express, { Application } from 'express';
import indexRoutes from './routes/index';
import { pool } from './db/database';
const app: Application = express();
import * as dotenv from 'dotenv';
import socketIO from 'socket.io'
import cors from 'cors';
import http from 'http'
import fs from 'fs';

let server = new http.Server(app);
let io = new socketIO.Server(server);

// middlewares
app.use(cors());
dotenv.config();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

io.on('connection', (socket: socketIO.Socket) => {
    console.log('a user connected : ' + socket.id)

    socket.emit('message', 'Hello ' + socket.id)

    // io.to("room1").emit('message', { user: user.name, text: message });

    socket.broadcast.emit(
        'message',
        'Everybody, say hello to ' + socket.id
    )

    socket.on('sendMessage', (message, callback) => {
        // const user = getUser(socket.id);
    
        io.to(socket.id).emit('message', {id:socket.id,  text: message });
    
        callback();
      });
    
    socket.on("file", (message, callback) => {
        console.log("received base64 file from server: " + message.fileName);
        const id = message.id;
        io.to(id).emit('image',  
          {
            file: message.file,
            fileName: message.fileName,
          }
        );
        callback();
      });
  

    socket.on("send-img", (image) => {
        const splitted = image.split(';base64,');
        const format = splitted[0].split('/')[1];
        fs.writeFileSync('./uploads' + format, splitted[1], { encoding: 'base64' });
    });

    socket.on('disconnect', function () {
        console.log('socket disconnected : ' + socket.id)
    })
})

app.use(indexRoutes);
app.get('/', (req, res) => {
    res.status(200).send('Hello world')
})
// const connection = pool.connect();
pool.connect((err) => {
    if (err) return console.log(`erron on ${err}`)
    console.log('connection success')
})

// console.log(connection)

server.listen(process.env.PORT||8080,()=>{
    console.log('Server on port', 8080);
});