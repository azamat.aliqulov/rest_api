import { Pool } from 'pg';
import * as dotenv from 'dotenv';

dotenv.config();

export const pool = new Pool({
    user: process.env.POSTGRES_USER||'postgres',
    host: process.env.POSTGRES_HOST||'postgres',
    password: process.env.POSTGRESS_PASSWORD||'newPassword',
    database: process.env.POSTGRES_DATABASE||'firstapi',
    port: Number(process.env.POSTGRES_PORT)||5432
});