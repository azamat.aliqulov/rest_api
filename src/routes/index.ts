import {Router} from 'express';
const router = Router();
import { checkJwt } from '../middlewares/checkJwt';
import { upload } from '../helpers/file.uploads';

import { getUsers, signIn, signUp, updateUser, deleteUser } from '../controllers/index.controller';

router.get('/users', getUsers);
router.get('/signin', signIn);
router.post('/signup', signUp);
router.put('/users/:id',[checkJwt], updateUser);
router.delete('/users/:id', deleteUser);
router.post('/file', upload.single('avatar'),  (req, res, next)=> {
    const file = req.file
    if (!file) {
        return res.status(400).send('Internal server error')
    }
    res.status(200).send('Your file has beesn sussessfully added')
  })

export default router;