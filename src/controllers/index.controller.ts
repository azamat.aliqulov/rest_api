import { Request, Response } from 'express';
import { pool } from '../db/database';
import { QueryResult } from 'pg';
import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";
import config from '../config/config';

//this endpoint used for test
export const getUsers = async (req: Request, res: Response): Promise<Response> => {
    try {
        const response: QueryResult = await
            pool.query('SELECT * FROM users ORDER BY id ASC');
        // console.log(response.rows[0]['name'])
        return res.status(200).json(response.rows);
    } catch (e) {
        console.log(e);
        return res.status(500).json('Internal Server error');
    }
};

export const signIn = async (req: Request, res: Response): Promise<Response> => {
    try {
        // const id = parseInt(req.params.id);
        let { email, password } = req.body;
        if (!(email && password)) {
            return res.status(400).send('Passwrord and email are required');
        }
        const hashPassword = bcrypt.hashSync(password, 8);
        const response: QueryResult = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
        // if(response.rowCount!=1){
        //     return res.status(400).json({
        //         message: 'User does not exist'
        //     })
        // }
        // Validate if user exist in our database
        if (response.rows.length == 0) {
            return res.status(400).json({
                message: 'User Dont match'
            })
        }
        if ((await bcrypt.compare(hashPassword, response.rows[0]['password']))) {
            return res.status(400).json({
                message: 'Password not matched'
            })
        };
        // Create token
        const token = jwt.sign({ name: response.rows[0]['name'], email: response.rows[0]['email'] }, config.jwtSecret, {
            expiresIn: "1h"
        });
        return res.json({ data: response.rows, token: token });
    } catch (error) {
        return res.status(400).json({error:error});
    }
}

export const signUp = async (req: Request, res: Response) => {

    try {
        const { name, email, password } = req.body;

        const check: QueryResult = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
        if (check.rowCount >= 1) {
            return res.status(400).json({
                message: 'User Already Added '
            })
        }

        const hashPassword = bcrypt.hashSync(password, 8);
        const token = jwt.sign({ name: name, email: email }, config.jwtSecret, {
            expiresIn: "1h"
        });
        const response = await pool.query('INSERT INTO users (name, email,password) VALUES ($1, $2,$3)', [name, email, hashPassword]);
        res.json({
            message: 'User Added successfully',
            body: {
                user: { name, email, token }
            }
        })
    } catch (error) {
        console.log(error)
    }
};

export const updateUser = async (req: Request, res: Response) => {

    try {
        const id = parseInt(req.params.id);

        const { name, email } = req.body;


        const response = await pool.query('UPDATE users SET name = $1, email = $2 WHERE id = $3', [
            name,
            email,
            id
        ]);

        res.json({
            message: 'User Updated Successfully',
            data: response
        })
    } catch (error) {
        console.log(error)
    }
};

export const deleteUser = async (req: Request, res: Response) => {

    try {
        const { email } = req.body;

        const response = await pool.query('DELETE FROM users where email = $1', [
            email
        ]);

        res.json({
            message: `User ${email} deleted Successfully`,
            data: response
        });
    } catch (error) {
        console.log(error)
    }
};