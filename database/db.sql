-- CREATE DATABASE firstapi;
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(40),
    email TEXT,
    password TEXT,
    createdAt date DEFAULT (now())
);
INSERT INTO users (name, email, password)
    VALUES ('joe', 'joe@ibm.com', 'aefjewfjmpwe;o'),
    ('ryan', 'ryan@faztweb.com' , 'fjweifjwpfw');
select * from users;